# README #

### What is this repository for? ###

* De Bruijn Sequence Generator
* Version 1.0


### How to cite ###

If you use this code please cite the following paper:


*Designing Uniquely Addressable Bio-orthogonal Synthetic Scaffolds for DNA and RNA Origami*
Jerzy Kozyra, Alessandro Ceccarelli, Emanuela Torelli, Annunziata Lopiccolo, Jing-Ying Gu, Harold Fellermann, Ulrich Stimming, and Natalio Krasnogor
**ACS Synthetic Biology** 2017 6 (7), 1140-1149
DOI: 10.1021/acssynbio.6b00271 

link:https://pubs.acs.org/doi/full/10.1021/acssynbio.6b00271


### Running Example ###

(Added by Ben Shirt-Ediss, July 2021)

cd src
javac DBSG.java
java DBSG.java -k 7 -n 2 -l 7000 -o myDBS.txt 	(for example)

Where:
	k is order of sequence
	l is length of sequence to generate
	n is number of sequences to generate (sequences are randomly chosen from those available)

Sequences are output to myDBS.txt

Note that LINEAR De Bruijn sequences are generated. These sequences don't have the correct k-order when bent around into a circle.

Specifying k puts a limit value on the sequence length l:

k		max len DBS (l)		max num DBS
1		4					6
2		16					20736
3		64					(a VERY large number)
4		256					(a VERY large number)
5		1024				(a VERY large number)
6		4096				(a VERY large number)
7		16384				(a VERY large number)
8		65536				(a VERY large number)

If l entered is larger than in the table above, sequences are truncated to (approximately) the value of l in the table above.

The number of sequences n is not constrained, as this is a stochastic choice from the total number of sequences in the table above.

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Benjamin Shirt-Ediss (email: benjamin.shirt-ediss@newcastle.ac.uk)
* Natalio Krasnogor (email: natalio.krasnogor@newcastle.ac.uk)