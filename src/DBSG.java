package main;

import graph.TabuGraph;

import java.util.ArrayList;
import java.util.List;

import utilities.DNA;
import utilities.FileReader;
import utilities.OutputWriter;
import utilities.SequenceTrimmer;

public class DBSG {

	// example usage: java DBSG.java -k 7 -n 10 -l 7000 -o myDBS.txt

	/** order k of generated De Bruijn sequences */
	public static int K = 7; //
	/** number of sequences to generate */
	public static int N = 10; //
	/** length of sequences */
	public static int L = 7000; //
	/** where sequences will be written */
	public static String OUTPUT_FILENAME = "myDBS.txt";
	/** where to get constraining sequences */
	public static String DATA_FILENAME = null;

	public static String ERROR_MESSAGE = "Too few arguments. Usage is: DBSG.java -k <order> -n <num_sequences> -l <trimmed_length> -o <output_filename> -d <data_filename>";
	public static String ERROR_MESSAGE_K = "Illegal arguments. Usage is: DBSG.java -k <order> -n <num_sequences> -l <trimmed_length> -o <output_filename> -d <data_filename>";

	public static void parseFlags(String[] args) {

		if (args.length < 4) {
			System.err.println(ERROR_MESSAGE);
			System.exit(1);
		}

		for (int i = 0; i < args.length; i += 2) {
			if ("-k".equals(args[i].trim())) {
				K = Integer.parseInt(args[i + 1].trim());
			}
			if ("-n".equals(args[i].trim())) {
				N = Integer.parseInt(args[i + 1].trim());
			}
			if ("-l".equals(args[i].trim())) {
				L = Integer.parseInt(args[i + 1].trim());
			}
			if ("-o".equals(args[i].trim())) {
				OUTPUT_FILENAME = args[i + 1].trim();
			}
			if ("-d".equals(args[i].trim())) {
				DATA_FILENAME = args[i + 1].trim();
			}
		}

		if (K <= 0 || N <= 0 || L <= 0) {
			System.err.println(ERROR_MESSAGE_K);
			System.exit(1);
		}
	}

	public static void main(String[] args) {

		parseFlags(args);

		List<String> alphabet = DNA.getAlphabet();

		TabuGraph graph = new TabuGraph(alphabet, K);

		if (DATA_FILENAME != null) {
			List<String> tabu = new ArrayList<String>();
			for (String line : FileReader.readFile(DATA_FILENAME)) {
				tabu.add(line.split(",")[0]);
			}
			graph.removeTabu(tabu);
		}

		List<String> sequences = new ArrayList<String>();
		while (sequences.size() < N) {
			long seed = sequences.size();
			String sequence = graph.traverseRandomly(seed);
			if (L > 0 && L < Math.pow(4, K)) {
				sequence = SequenceTrimmer.trim(sequence, K, L);
				if (sequence.length() != L) {
					continue;
				}
			}
			sequences.add(sequence);
		}

		if (OUTPUT_FILENAME != null)
			OutputWriter.writeToFile(OUTPUT_FILENAME, sequences);
		else
			for (String seq : sequences)
				System.out.println(seq);
	}
}
