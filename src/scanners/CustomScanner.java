package scanners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import utilities.DNA;

public class CustomScanner implements Scanner {

	private List<String> tabuList = null;

	public CustomScanner(String sequence) {
		this(Arrays.asList(sequence));
	}

	public CustomScanner(List<String> tabuList) {
		this.tabuList = new ArrayList<String>();
		for (String tabu : tabuList) {
			if (DNA.isValidSequence(tabu)) {
				this.tabuList.add(tabu);
			}
		}
	}

	@Override
	public boolean containsMatches(String sequence) {
		for (String site : tabuList) {
			if (sequence.contains(site)) {
				return true;
			}
		}
		return false;
	}
}
