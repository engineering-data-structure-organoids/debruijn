package scanners;

public interface Scanner {

	public boolean containsMatches(String sequence);

}
