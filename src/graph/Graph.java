package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

public class Graph {

	private int size = 0;

	private Random rdm = null;

	private Edge start = null;
	private SortedMap<String, Edge> allEdges;

	public Graph(List<String> alphabet, int length) {

		this.size = (int) Math.pow(alphabet.size(), length) + length - 1;

		SortedMap<String, Edge> edges = new TreeMap<String, Edge>();
		allEdges = edges;

		for (int i = 0; i < Math.pow(alphabet.size(), length); i++) {

			Edge edge = new Edge(i, alphabet, length);
			edges.put(edge.getName(), edge);
			// System.out.println(edge.getName());
		}

		if (start == null) {
			setStart();
		}
		connect(edges);
	}

	public SortedMap<String, Edge> getAllEdges() {
		return allEdges;
	}

	public void setAllEdges(SortedMap<String, Edge> allEdges) {
		this.allEdges = allEdges;
	}

	public boolean isStartSet() {
		return start != null;
	}

	public void setStart() {
		Iterator<Edge> itr = getAllEdges().values().iterator();
		if (itr.hasNext()) {
			start = itr.next();
		}
		// System.out.println(getAllEdges().size() + " " + start.getName());
	}

	private void connect(SortedMap<String, Edge> edges) {

		for (Edge first : edges.values()) {
			for (Edge second : edges.values()) {
				if (first.matches(second)) {
					first.connect(second);
				}
			}
		}
	}

	public void traverse() {

		System.out.println();

		List<Edge> seen = new ArrayList<Edge>();
		seen.add(start);

		traverse(start, seen, start.getName());
	}

	public boolean traverse(Edge current, List<Edge> seen, String sequence) {

		while (sequence.length() < size) {

			boolean deadEnd = true;

			if (isDeadEnd(current, seen)) {
				return true;
			}

			for (Edge next : current.getOut()) {

				if (sequence.contains(next.getName())) {
					continue;
				}

				List<Edge> tempSeen = new ArrayList<Edge>();
				tempSeen.addAll(seen);
				tempSeen.add(next);

				String tempSequence = sequence + next.lastSymbol();

				if (traverse(next, tempSeen, tempSequence)) {
					continue;
				}

				deadEnd = false;
			}

			if (deadEnd) {
				return true;
			}
		}

		System.out.println(sequence);
		return true;
	}

	public String traverseRandomly(long seed) {

		rdm = new Random(seed);

		List<Edge> seen = new ArrayList<Edge>();

		List<Edge> remaining = new ArrayList<Edge>();
		remaining.addAll(allEdges.values());

		Edge current = start;

		seen.add(current);
		remaining.remove(current);

		// start random traverse
		while (!isDeadEnd(current, seen)) {

			Collections.shuffle(current.getOut(), rdm);

			for (Edge next : current.getOut()) {

				if (seen.contains(next)) {
					continue;
				}

				current = next;
				seen.add(current);
				remaining.remove(current);
				break;
			}
		}

		// complete the sequence with remaining parts (random insertions)
		while (!remaining.isEmpty()) {

			Edge edge = null;
			int index = -1;
			while (index < 0) {

				edge = remaining.get(rdm.nextInt(remaining.size()));
				Edge prev = edge.getIn().get(rdm.nextInt(edge.getIn().size()));
				index = seen.indexOf(prev);
			}

			index++;
			seen.add(index, edge);
			remaining.remove(edge);

			while (!edge.matches(seen.get(index + 1))) {

				Edge next = edge.getOut()
						.get(rdm.nextInt(edge.getOut().size()));

				if (remaining.contains(next)) {
					index++;
					seen.add(index, next);
					remaining.remove(next);
					edge = next;
				}
			}
		}
		return getSequence(seen);
	}

	public boolean isDeadEnd(Edge edge, List<Edge> seen) {

		boolean deadEnd = true;
		for (Edge next : edge.getOut()) {
			if (!seen.contains(next)) {
				deadEnd = false;
				break;
			}
		}
		return deadEnd;
	}

	public static String getSequence(List<Edge> edges) {
		if (edges.isEmpty()) {
			return "";
		}
		StringBuffer strBuf = new StringBuffer();
		strBuf.append(edges.get(0).getPrefix());
		for (Edge e : edges) {
			strBuf.append(e.lastSymbol());
		}
		return strBuf.toString();
	}

	public static void printEdges(List<Edge> edges) {
		for (Edge e : edges) {
			System.out.print(e.getName() + " ");
		}
		System.out.println();
	}

	public static void printSequence(List<Edge> edges) {
		if (edges.isEmpty()) {
			return;
		}
		System.out.print(edges.get(0).getPrefix());
		for (Edge e : edges) {
			System.out.print(e.lastSymbol());
		}
		System.out.println();
	}
}
