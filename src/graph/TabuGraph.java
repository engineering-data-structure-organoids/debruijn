package graph;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class TabuGraph extends Graph {

	public TabuGraph(List<String> alphabet, int length) {
		super(alphabet, length);
	}

	public List<List<Edge>> findCycles(String edgeName) {

		Edge first = getAllEdges().get(edgeName);
		return findCycles(first);
	}

	public List<List<Edge>> findCycles(Edge first) {

		List<List<Edge>> memory = new ArrayList<List<Edge>>();

		List<Edge> cycle = new ArrayList<Edge>();
		cycle.add(first);

		int limit = 1;
		while (memory.isEmpty()) {
			findCycles(cycle, first, memory, limit);
			limit *= 2;
		}
		return memory;
	}

	public void findCycles(List<Edge> cycle, Edge current,
			List<List<Edge>> memory, int limit) {

		if (current.matches(cycle.get(0))) {
			memory.add(cycle);
			return;
		}

		for (Edge next : current.getOut()) {

			if (cycle.size() >= limit) {
				break;
			}

			List<Edge> tempCycle = new ArrayList<Edge>();
			tempCycle.addAll(cycle);
			tempCycle.add(next);

			if (!cycle.contains(next)) {
				findCycles(tempCycle, next, memory, limit);
			}
		}
	}

	public List<Edge> shortestCycle(Edge first) {
		List<Edge> shortest = new ArrayList<Edge>();
		for (List<Edge> cycle : findCycles(first)) {
			if (shortest.isEmpty()) {
				shortest = cycle;
			} else if (shortest.size() > cycle.size()) {
				shortest = cycle;
			}
		}
		// printEdges(shortest);
		return shortest;
	}

	public void removeTabu(List<String> tabu) {

		SortedMap<String, Edge> remaining = new TreeMap<String, Edge>();
		remaining.putAll(getAllEdges());

		for (Edge edge : getAllEdges().values()) {

			if (isTabu(edge, tabu)) {

				// ignore if already removed
				if (!remaining.containsKey(edge.getName())) {
					continue;
				}

				// remove and disconnect
				remaining.remove(edge.getName());
				disconnectEdge(edge);

				// fix graph: remove and disconnect the cycle
				for (Edge toRemove : shortestCycle(edge)) {
					remaining.remove(toRemove.getName());
					disconnectEdge(toRemove);
				}
			}
		}

		setAllEdges(remaining);
		setStart();
	}

	private void disconnectEdge(Edge edge) {

		for (Edge e : getAllEdges().values()) {

			if (e.getOut().contains(edge)) {
				e.getOut().remove(edge);
			}
			if (e.getIn().contains(edge)) {
				e.getIn().remove(edge);
			}
		}
	}

	private boolean isTabu(Edge edge, List<String> tabu) {
		for (String str : tabu) {
			if (edge.getName().contains(str)) {
				return true;
			}
		}
		return false;
	}

	public static List<String> trimTabu(List<String> tabu, int n) {

		List<String> trimmedTabu = new ArrayList<String>();

		for (String str : tabu) {
			if (str.length() > n) {
				trimmedTabu.add(str.substring(0, n));
			} else {
				trimmedTabu.add(str);
			}
		}
		return trimmedTabu;
	}
}
