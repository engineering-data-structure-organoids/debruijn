package graph;

import java.util.ArrayList;
import java.util.List;

public class Edge implements Comparable<Edge> {

	public static int symbolSize = 0;

	private String name = "";

	private List<Edge> in;
	private List<Edge> out;

	public Edge(int i, List<String> alphabet, int length) {

		in = new ArrayList<Edge>();
		out = new ArrayList<Edge>();

		symbolSize = alphabet.get(0).length();

		while (i > 0) {
			name = alphabet.get(i % alphabet.size()) + name;
			i /= alphabet.size();
		}
		while (name.length() < length * symbolSize) {
			name = alphabet.get(0) + name;
		}
	}

	public String getName() {
		return name;
	}

	public String getPrefix() {
		return name.substring(0, name.length() - symbolSize);
	}

	public String getSuffix() {
		return name.substring(1);
	}

	public String fisrtSymbol() {
		return name.substring(0, 1);
	}

	public String lastSymbol() {
		return name.substring(name.length() - symbolSize);
	}

	public boolean matches(Edge edge) {
		return name.substring(symbolSize).equals(
				edge.getName().substring(0,
						edge.getName().length() - symbolSize));
	}

	public boolean matches(String edge) {
		return name.substring(symbolSize).equals(
				edge.substring(0, edge.length() - symbolSize));
	}

	public void connect(Edge edge) {
		this.out.add(edge);
		edge.in.add(this);
	}

	public void disconnect() {
		in = new ArrayList<Edge>();
		out = new ArrayList<Edge>();
	}

	public void disconnect(Edge edge) {
		in.remove(edge);
		out.remove(edge);
	}

	public List<Edge> getIn() {
		return in;
	}

	public List<Edge> getOut() {
		return out;
	}

	@Override
	public int compareTo(Edge edge) {
		return this.getName().compareTo(edge.getName());
	}

	@Override
	public String toString() {
		return getName();
	}

	// public static void main(String args[]) {
	//
	// List<String> alphabet = new ArrayList<String>();
	// alphabet.add("0");
	// alphabet.add("1");
	// Edge a = new Edge(0, alphabet, 3);
	// Edge b = new Edge(1, alphabet, 3);
	// Edge c = new Edge(2, alphabet, 3);
	// Edge d = new Edge(3, alphabet, 3);
	// Edge e = new Edge(4, alphabet, 3);
	//
	// System.out.println(a.getName());
	// System.out.println(b.getName());
	// System.out.println(c.getName());
	// System.out.println(d.getName());
	// System.out.println(e.getName());
	//
	// System.out.println();
	//
	// System.out.println(a.matches(a));
	// System.out.println(a.matches(b));
	// System.out.println(e.matches(a));
	// System.out.println(e.matches(b));
	// System.out.println(c.matches(e));
	// System.out.println(b.matches(d));
	//
	// System.out.println();
	//
	// System.out.println(a.matches(c));
	// System.out.println(c.matches(c));
	// System.out.println(d.matches(a));
	// System.out.println(e.matches(c));
	// }
}