package utilities;

import java.util.Arrays;
import java.util.List;

public class DNA {

	public static List<String> getAlphabet() {
		return Arrays.asList(new String[] { "A", "C", "G", "T" });
	}

	public static String reverse(String sequence) {
		return new StringBuffer(sequence).reverse().toString();
	}

	public static String complement(String sequence) {
		StringBuffer strBuf = new StringBuffer();
		for (int i = 0; i < sequence.length(); i++) {
			strBuf.append(complement(sequence.charAt(i)));
		}
		return strBuf.toString();
	}

	public static String reverseComplement(String sequence) {
		StringBuffer strBuf = new StringBuffer();
		for (int i = sequence.length(); i > 0; i--) {
			strBuf.append(complement(sequence.charAt(i - 1)));
		}
		return strBuf.toString();
	}

	public static boolean isNucleotide(char nt) {
		switch (nt) {
		case 'A':
		case 'T':
		case 'C':
		case 'G':
		case 'a':
		case 't':
		case 'c':
		case 'g':
			return true;
		default:
			return false;
		}
	}

	private static char complement(char nt) {
		switch (nt) {
		case 'A':
			return 'T';
		case 'T':
			return 'A';
		case 'C':
			return 'G';
		case 'G':
			return 'C';
		case 'a':
			return 't';
		case 't':
			return 'a';
		case 'c':
			return 'g';
		case 'g':
			return 'c';
		default:
			throw new IllegalArgumentException();
		}
	}

	public static boolean isValidSequence(String sequence) {
		for (int i = 0; i < sequence.length(); i++) {
			if (isNucleotide(sequence.charAt(i))) {
				continue;
			} else {
				return false;
			}
		}
		return true;
	}
}
