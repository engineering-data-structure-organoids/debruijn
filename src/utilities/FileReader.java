package utilities;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileReader {

	public static final String DEFAULT_PATH = "input" + File.separator;

	public static List<String> readFile(String filename) {

		List<String> lines = new ArrayList<String>();

		try {
			FileInputStream fstream = new FileInputStream(DEFAULT_PATH
					+ filename);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null) {
				lines.add(strLine);
			}
			in.close();

		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}

		return lines;
	}
}
