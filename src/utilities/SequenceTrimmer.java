package utilities;

import graph.TabuGraph;

import java.util.List;

public class SequenceTrimmer {

	public static String trim(String sequence, final int SIZE,
			final int TARGET_LENGTH) {

		final int CURRENT_LENGTH = sequence.length();
		int currentBalance = CURRENT_LENGTH - TARGET_LENGTH;

		for (int origin = 0; origin < sequence.length() - SIZE; origin++) {

			for (int target = origin + 1; target < sequence.length() - SIZE; target++) {

				if (sequence.substring(origin, origin + SIZE - 1).equals(
						sequence.substring(target, target + SIZE - 1))) {

					int distance = target - origin;
					if (currentBalance - distance >= 0) {
						sequence = sequence.substring(0, origin) + ""
								+ sequence.substring(target);

						currentBalance -= distance;
					}
				}
			}
		}
		return sequence;
	}

	public static void main(String[] args) {

		final int SIZE = 6;

		List<String> alphabet = DNA.getAlphabet();

		TabuGraph graph = new TabuGraph(alphabet, SIZE);

		for (long SEED = 0; SEED < 1000; SEED++) {

			String sequence = graph.traverseRandomly(SEED);

			String trimmed = SequenceTrimmer.trim(sequence, 6, 2484);

			System.out.println(trimmed.length() + " " + trimmed);
		}
	}
}
