package utilities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class OutputWriter {

	public static void writeToFile(String filename, List<String> lines) {

		try {
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);

			for (int i = 0; i < lines.size(); i++) {
				out.write(">DBS_scaffold_" + (i + 1) + "\n");
				out.write(lines.get(i) + "\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
